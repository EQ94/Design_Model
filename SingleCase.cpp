/*
	单例模式也称为单件模式、单子模式，可能是使用最广泛的设计模式。其意图是保证一个类仅有一个实例，并提供一个访问它的全局访问点，该实例被所有程序模块共享，
有很多地方需要这样的功能模块，如系统的日志输出，GUI应用必须是单鼠标，MODEM的链接需要一条且只需要一条电话线，操作系统只能有一个窗口管理器，一台PC连一个键盘

实现：
	定义一个单例类，使用类的私有静态指针变量指向类的唯一实例，并用一个公有的静态方法获取该实例
功能：
	单例模式通过类本身来管理其唯一实例，这种特性提供了解决问题的方法。唯一的实例是类的一个普通对象，但设计这个类时，让它只能创建一个实例并提供对此实例的全局访问。
唯一实例类Singleton在静态成员函数中隐藏创建实例的操作。习惯上把这个程序函数叫做Instance（），它的返回值是唯一实例的指针。
说明：
	用户访问唯一实例的方法只有GetInstance()成员函数。如果不通过这个函数，任何创建实例的尝试都讲失败，因为类的构造函数是私有的。
GetInstance()使用懒惰初始化，也就是它的返回值是当这个函数首次被访问时被创建的。所有GetInstance()之后的调用都返回相同实例的指针。
*/
/*
Static类成员：
	类的对象构建过程不会动刀static变量和函数，因为它存在静态内存，程序加载进内存的时候它就存在；而对象生命周期不同
static数据成员:
	static数据成员要在程序一开始运行时就必须存在。因为函数在程序运行中被调用，所以静态数据成员不能再任何函数内分配空间和初始化；
	static数据成员要实际的分配空间，故不能再类的声明中定义，也不能在头文件中类声明的外部定义，因为那会造成在多个使用该类的源文件中，对其重复定义
	static数据成员必须在类定义体的外部定义（正好一次）。不想普通数据成员，static成员不是通过类构造函数进行初始化，而是应该在定义时进行初始化。
* 在类中，静态成员可以实现多个对象之间的数据共享，并且使用静态数据成员还不会破坏隐藏的原则，即保证了安全性。因此静态成员那是类的所有对象中共享的成员，而不是某个对象的成员
* 静态数据成员可以节省内存，因为它是所有对象所公有的，因此，对多个对象来说，静态数据成员只存储一处，供所有对象共用。静态数据成员的值对每个对象都是一样，但他的值是可以更新的。只要对
静态数据成员的值更新一次，保证所有对象存取更新后的相同的值，这样可以提高时间效率。
* 初始化格式为 <数据类型><类名>::<静态数据成员名>=<值>。初始化在类体外进行，而前面不加static，以免与一般静态变量或对象相混淆。

static成员函数：
* 在静态成员函数的实现中不能直接引用类中说明的非静态成员，可以引用类中说明的静态成员。如果静态成员函数中要引用非静态成员，可通过对象来引用
* 静态成员函数，不属于任何一个具体的对象，那么在类的具体对象声明之前就已经有了内存区，而非静态数据成员你还没有分配内存空间，那么在静态成员函数中使用非静态成员函数，就好像没有声明一个变量却提前使用他一样
* static成员函数没有this指针，不能被声明为const，不能被声明为虚函数

*/
/*
一个妥善的方法删除该实例：
	让这个类自己知道在合适的时候把自己删除，就是说把删除自己的操作挂在操作系统中的某个合适的点上，使其在恰当的时候被自动执行。
	程序在结束的时候，系统会自动析构所有的全局变量。事实上，系统也会析构所有类的静态成员变量，就像这些静态成员也是全局变量一样。
利用这个特征，我们可以在单例类中定义一个这样的静态成员变量，而它的唯一工作就是在析构函数中删除单例类的实例。

程序中 CGarbo 被定义为CSingleton的私有内嵌类，以防该类被在其他地方滥用。
程序运行结束时，系统会调用CSingleton的静态成员Garbo的析构函数，该析构函数会删除单例的唯一实例

使用这种方法释放单例对象有以下特征：
	在单例类内部定义专有的嵌套类；
	在单例类内定义私有的专门用于释放的静态成员；
	利用程序在结束时析构全局变量的特性，选择最终的释放时机；
	使用单例的代码不需要任何操作，不必关心对象的释放。

注意：
	单例中的new的对象需要delete释放；
	delete释放对象的时候才会调用对象的析构函数
	如果在析构函数里调用delete，那么程序结束时，根本进不去析构函数
	如果程序结束能自动析构，那么就会造成一个析构的循环，所以new对应于delete
*/
#include<iostream>
#include<string>
#include<vector> 

using namespace std; 
// 懒汉式（创建的时候才去new实例），以时间换取空间，线程不安全
class CSingleton
{
private:
	CSingleton() // 私有化构造函数
	{
		cout << "CSingleton Construct" << endl;
	};  
	~CSingleton() // 私有析构函数
	{
		cout << "CSingleton Destruct" << endl;
	}; 
	class CGarbo
	{
	public:
		~CGarbo()   // 它的唯一工作就是在析构函数中删除CSingleton的实例
		{
			if (CSingleton::m_pInstance)
				delete CSingleton::m_pInstance;
		}
	};

	static CGarbo Garbo;  // 定义一个静态成员变量，程序结束时，系统会自动调用它的析构函数

	static CSingleton *m_pInstance;  // 使用类的私有静态指针变量指向类的唯一实例

public:
	static CSingleton *GetInstance(); // 用一个公有的静态方法获取该实例
};
CSingleton* CSingleton::m_pInstance = NULL;
CSingleton* CSingleton::GetInstance()
{
	if (m_pInstance == NULL)			   // 判断是否是第一次调用
		m_pInstance = new CSingleton();    // 指向类的唯一实例
	return m_pInstance;
}

CSingleton::CGarbo CSingleton::Garbo;  // 嵌套静态类成语 ，必须类外声明

// 懒汉式线程安全法：
/*
所谓双重检查加锁机制，指的是：并不是每次进入getInstance方法都需要同步，而是先不同步，进入方法过后，先检查实例是否存在，
如果不存在才进入下面的同步块，这是第一重检查。进入同步块过后，再次检查实例是否存在，如果不存在，就在同步的情况下创建一个实例，
这是第二重检查。这样一来，就只需要同步一次了，从而减少了多次在同步情况下进行判断所浪费的时间。
*/
/*
// 没有phread.h头文件，无法使用pthread_mutex_lock函数
class singleton_lock
{
public:
	static pthread_mutex_t mutex;
	static singleton_lock* getinstance()
	{
		if (instance == NULL)
		{
			pthread_mutex_lock(&mutex);  // 加锁
			if (instance == NULL)
				instance = new singleton_lock();
			pthread_mutex_unlock(&mutex);
		}
		return instance;
	}
	class GGarbo
	{

	};
private:
	singleton_lock();
	static singleton_lock* instance;
	static GGarbo ggarbo;
};
*/
class singlenton_hugry
{
public:
	static singlenton_hugry* getInstance()
	{
		return instance;
	}
	class GGarbo
	{
	public:	
		~GGarbo()
		{
			if (singlenton_hugry::instance != NULL)
			{
				cout << "delete singlenton_hungry" << endl;
				delete singlenton_hugry::instance;
			}
		}
	};
private:
	singlenton_hugry() {};
	static singlenton_hugry* instance;
	static GGarbo ggarbo;
};
singlenton_hugry* singlenton_hugry::instance = new singlenton_hugry(); // 饿汉模式，用空间换时间
singlenton_hugry::GGarbo singlenton_hugry::ggarbo;
int main()
{
	CSingleton *p1 = CSingleton::GetInstance();
	CSingleton *p2 = p1->GetInstance();
	CSingleton & ref = *CSingleton::GetInstance();

	system("pause");
	return 0;
}