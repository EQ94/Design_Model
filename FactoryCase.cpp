/*  工厂模式
引子：
	1：为了提高内聚和松耦合，经常会抽出一些类的公共接口以形成抽象基类或者接口。这样可以通过声明一个指向基类的指针来指向实际的子类实现，
从而达到多态的目的。这里很容易出现的一个问题 n 多的子类继承自抽象基类，我们不得不在每次要用到子类的地方就编写诸如 new xxx 的代码。
这里带来两个问题： 客户程序员必须知道实际子类的名称（当系统复杂后，命名将是一个很不好处理的问题，为了处理可能的名字冲突，有的明明可能
并不是具有很好的可读性和可记忆性）。从而程序的扩展性和维护变得越来越困难。
	2：还有一种情况就是父类中并不知道具体要实例化哪一个具体的子类，这里的意思为：假设我们在类A中要使用到类B，B是一个抽象父类，在A中并
不知道具体要实例化哪一个B的子类，但是在类A的子类D中是可以知道的。在A中我们没有办法直接使用类似于 new xxx 的语句，因为根本不知道是 xxx
是什么。

目的：
	工厂模式的目的：
		1） 定义创建对象的接口，封装了对象的创建
		2） 使得具体化类的工作延迟到了子类中

分类：
	简单工厂模式（Simple Factory），工厂方法模式（Factory Method），抽象工厂模式（Abstract Factory）

*/
/*  简单工厂模式：
1）工厂类角色： 这是本模式的核心，含有一定的商业逻辑和判断逻辑
2）抽象产品角色： 一般是具体产品继承的父类或者实现的接口
3）具体产品角色：工厂类所常见的对象就是此角色的实例

目的：
	定义一个用于创建对象的接口
缺点：
	对修改不封闭，新增件产品时需要修改工厂。

情形描述：
	有一个肥皂厂，声场各种肥皂，有舒肤佳，夏士莲纳爱斯等，给这个肥皂厂建模
*/
//#include<iostream>
//using namespace std;
//enum PRODUCTTYPE {SFJ,XSL,NAS};
//class soapBase												 // 抽象产品类：具体产品继承的父类或者实现的接口
//{
//public:
//	soapBase() { cout << "soapBase Construct" << endl; }
//	virtual ~soapBase() { cout << "soapBase Destruct" << endl; };
//	virtual void show() = 0;   // 抽象基类
//};
//class SFJSoap :public soapBase								 // 具体产品类：工厂类所创建的对象就是此类角色的实例
//{
//public:
//	SFJSoap() { cout << "SFJSoap Construct" << endl; }
//	void show() { cout << "SFJ Soap!" << endl; }
//};
//
//class XSLSoap :public soapBase								//	具体产品类：工厂类所创建的对象就是此角色的实例
//{
//public:
//	XSLSoap() { cout << "XSLSoap Construct" << endl; }
//	void show() { cout << "SXK Soap!" << endl; }
//};
//
//class NASSoap :public soapBase
//{
//public:
//	NASSoap() { cout << "NASSoap Construct" << endl; }
//	void show() { cout << "NAS Soap!" << endl; }
//};
//class Factory													//  工厂类：模式的核心，含有一定的商业逻辑和判断逻辑
//{
//public:
//	Factory()
//	{
//		cout << "Factor Construct" << endl;
//	}
//	soapBase * creatSoap(PRODUCTTYPE type)  // soapBase 是抽象基类
//	{
//		switch (type)
//		{
//		case SFJ:
//			return new SFJSoap();
//			break;
//		case XSL:
//			return new XSLSoap();
//			break;
//		case NAS:
//			return new NASSoap();
//			break;
//		default:
//			break;
//		}
//	}
//};
//int main()
//{
//	Factory factory;      // Factory Construct
//	soapBase* pSoap1 = factory.creatSoap(SFJ); // soapBase Construct  SFJSoap Construct
//	pSoap1->show();       // SFJ Soap
//	soapBase* pSoap2 = factory.creatSoap(XSL); // soapBase Construct  XSLSoap Construct
//	pSoap2->show();       // XSL Soap
//	soapBase* pSoap3 = factory.creatSoap(NAS); // soapBase Construct  NASSoap Construct
//	pSoap3->show();		  // NAS Soap
//
//	delete pSoap1;        // soapBase Destruct  析构函数调用顺序与构造函数相反，应该先子类，后基类
//	delete pSoap2;        // soapBase Destruct
//	delete pSoap3;		  // soapBase Destruct
//
//	system("pause");
//	return 0;
//}

/*	工厂方法模式
具体情形：
	最近莫名肥皂需求激增，于是要独立每个生产线，每个生产线只生产一种肥皂

区别：
	工厂方法模式的应用并不是为了封装对象的创建，而是要把对象的创建放到子类中实现 : Factoy中只提供了对象创建的接口，
其实现将放在Factory的子类ConcreteFactory中进行。
	对于工厂方法模式的组成：
1）抽象工厂角色：这是工厂方法模式的核心，他与应用程序无关。是具体工厂角色必须实现的接口或者必须继承的父类
2）具体工厂角色：含有和具体业务逻辑有关的代码。由应用程序调用已创建对应的具体产品对象
3）抽象产品角色：具体产品继承的父类或者是实现的接口
4）具体产品角色：具体工厂角色所创建的对象就是此角色的实例

缺点：
	每增加一种产片，就需要增加一个对象的工厂。如果这家公司发展迅速，退出了很多新的处理器核，那么就要开设相应的新工厂。
在C++实现中，就是要定义一个个的工厂类额。显然，相比简单的工厂模式，工厂方法模式需要更多的类定义。
*/
//#include<iostream>
//using namespace std;
//enum SOAPTYPE{SFJ,XSL,NAS};
//class soapBase											// 抽象产品类
//{
//public:
//	soapBase() { cout << "soapBase Construct" << endl; }
//	virtual ~soapBase() {};
//	virtual void show() = 0;
//};
//class SFJSoap :public soapBase
//{
//public:
//	void show() { cout << "SFJ Soap!" << endl; }
//};
//class XSLSoap :public soapBase
//{
//public:
//	void show() { cout << "XSL Soap!" << endl; }
//};
//class NASSoap :public soapBase
//{
//public:
//	void show() { cout << "NAS Soap!" << endl; }
//};
//class FactoryBase
//{
//public:
//	FactoryBase(){ cout << "FactoryBase Construct" << endl; }
//	virtual soapBase* creatSoap() = 0;
//};
//class SFJFactory :public FactoryBase
//{
//public:
//  SFJFactory(){ cout << "SFJFactory Construct" << endl; }
//	soapBase* creatSoap() { return new SFJSoap(); }
//};
//class XSLFactory :public FactoryBase
//{ 
//public:
//	XSLFactory(){ cout << "XSLFactory Construct" << endl; }
//	soapBase* creatSoap() { return new XSLSoap(); }
//};
//class NASFactory :public FactoryBase
//{
//public:
//	NASFactory(){ cout << " NASFactory Construct" << endl; }
//	soapBase* creatSoap() { return new NASSoap(); }
//};
//int main()
//{
//	SFJFactory factory1;
//	soapBase* pSoap1 = factory1.creatSoap();
//	pSoap1->show();
//	XSLFactory factory2;
//	soapBase* pSoap2 = factory2.creatSoap();
//	pSoap2->show();
//	NASFactory factory3;
//	soapBase* pSoap3 = factory3.creatSoap();
//	pSoap3->show();
//
//	delete pSoap1;
//	delete pSoap2;
//	delete pSoap3;
//
//	system("pause");
//	return 0;
//}

/* 抽象工厂模式
具体情形：
	这个肥皂厂发现搞牙膏也很赚钱，决定做牙膏，牙膏有高档低档，肥皂也是，现在搞两个厂房，一个生产低档的牙膏和肥皂，一个生产高档的牙膏和肥皂。
比如：厂房一生产中华牙膏、纳爱斯，厂房二生产黑人牙膏和舒肤佳牙膏

抽象工厂模式：
	给客户提供一个接口，可以创建多个产品族中的产品对象，而且使用抽象工厂模式还要满足以下条件：
1）系统中有多个产品族，而系统一次只可能消费其中一族产品
2）同属于同一个产品族的产品以其使用

抽象工厂模式的组成（和工厂方法模式一样）：
1）抽象工厂角色：工厂刚发模式的核心，与应用程序无关。是具体工厂角色必须实现的接口或者必须继承的父类
2）具体工厂角色：含有和具体业务逻辑有关的代码。有应用程序调用已创建对应的具体产品的对象
3）抽象产品角色：具体产品继承的父类或者是实现的接口
4）具体产品角色：具体工厂角色所创建的对象就是此角色的实例
*/
#include<iostream>
using namespace std;
enum SOAP { SFJ, XSL, NAS };
enum TOOTHTYPE { HR, ZH };
class SoapBase												        // 抽象产品1角色
{
public:
	SoapBase() { cout << "SoapBase Construct" << endl; }
	virtual ~SoapBase() { cout << "SoapBase Destruct" << endl; };                      
	virtual void show() = 0;
};
class SFJSoap :public SoapBase										// 具体产品角色
{
public:
	SFJSoap() { cout << "SFJSoap Construct!" << endl; }
	void show() { cout << "SFJ Soap!" << endl; }
};
class NASSoap :public SoapBase										// 具体产品角色
{
public:
	NASSoap(){ cout << "NASSoap Construct!" << endl; }
	void show() { cout << "NAS Soap!" << endl; }
};  
class ToothBase														// 抽象产品2
{
public:
	ToothBase() { cout << "ToothBase Construct" << endl; }
	virtual ~ToothBase() { cout << "ToothBase Destruct" << endl; }
	virtual	void show() = 0;
};
class HRTooth :public ToothBase
{
public:
	void show() { cout << "Hei ren Toothpaste!" << endl; }
};
class ChinaTooth :public ToothBase
{
public:
	void show() { cout << "China Toothpaste!" << endl; }
};
////////// 工厂函数 
class FactoryBase													// 抽象工厂函数
{
public:
	FactoryBase() { cout << "FactoryBase Construct!" << endl; }
	virtual SoapBase *creatSoap() = 0;
	virtual ToothBase *creatToothpaste() = 0;
};
class FactoryA :public FactoryBase   // 创建对象					// 具体工厂1
{
public:
	FactoryA() { cout << "FactoryA Construct" << endl; }			// 工厂1创建两类产品的高档品牌
	SoapBase* creatSoap(){	
		return new SFJSoap();	
	}
	ToothBase* creatToothpaste() { 
		return new HRTooth(); 
	}
};
class FactoryB :public FactoryBase   // 创建另一组对象				// 具体工厂2
{
public:
	FactoryB() { cout << "FactoryB Construct!" << endl; }
	SoapBase* creatSoap() {											// 工厂2创建两类产品的低档品牌
		return new NASSoap();
	}
	ToothBase* creatToothpaste() { 
		return new ChinaTooth(); 
	}
};
int main()
{
	FactoryA factory1;				// 工厂类
	FactoryB factory2;
	SoapBase* pSoap1 = NULL;         // 没有创建对象，没有调用构造函数
	ToothBase* pToothpaste1 = NULL;

	pSoap1 = factory1.creatSoap();
	pToothpaste1 = factory1.creatToothpaste();

	pSoap1->show();
	pToothpaste1->show();

	SoapBase *pSoap2 = NULL;
	ToothBase *pToothpaste2 = NULL;

	pSoap2 = factory2.creatSoap();
	pToothpaste2 = factory2.creatToothpaste();

	pSoap2->show();
	pToothpaste2->show();

	delete pSoap1;
	delete pSoap2;
	delete pToothpaste1;
	delete pToothpaste2;

	system("pause");
	return 0;
}